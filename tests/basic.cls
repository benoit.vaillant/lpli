\ProvidesClass{basic}[2018/11/19 CV class]
\NeedsTeXFormat{LaTeX2e}
\LoadClass{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%PACKAGES%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage[sfdefault]{ClearSans}
\RequirePackage[T1]{fontenc}
\RequirePackage{tikz}
\RequirePackage{xcolor}
\RequirePackage[absolute,overlay]{textpos}
\RequirePackage{ragged2e}
\RequirePackage{marvosym}
\RequirePackage{parskip}

\RequirePackage[\langoption]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage{pstricks,pst-text,pst-plot}
\RequirePackage{colortbl}
\RequirePackage{amsmath}
\RequirePackage{enumitem}
\RequirePackage{wasysym}
\RequirePackage{pifont}
\RequirePackage{turnstile}
\RequirePackage{fontawesome}
\RequirePackage{diagbox}
\RequirePackage[most]{tcolorbox}
\RequirePackage[nodayofweek]{datetime}

%% maths and other symbols
\RequirePackage{amssymb}
\RequirePackage{fourier}

\RequirePackage{lettrine}

\RequirePackage[hidelinks]{hyperref}


%% must be loaded _after_ hyperref
\RequirePackage[left=7.3cm,top=0.1cm,right=0.5cm,bottom=0.2cm,nohead,nofoot]{geometry}
\RequirePackage{multimedia}

%% fancy things
\usetikzlibrary{positioning} %% this one is missed for now
