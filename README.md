# lpli

## Synopsis

    lpli [-chlr] [FILE]...

## Description

`lpli` (short for LaTeX Project License Inventory) is a basic script
that parses LaTeX files (usually a project) and retrieves the licenses
of packages or classes used if it can. The goal is to help in the
validation that the current project license complies with them, or
choose a valid one for the project.

It may output two kind of lists:
- the list of licenses with each class/package under this license,
- the list of class/packages with their respective licenses.

For now, only checks agains CTAN are done.

By default, `-cl` is assumed if no option specified, and the `.`
directory is used if no file is given (`r` is then set).

## Options

`-c`

Output the list of licenses with each class/package under this license

`-h`

Displays help

`-l`

Output the list of class/packages with their respective licenses

`-r`

Recurse on directories in the list of [FILE]s

`[FILE]...`

A list of files (or directories if recursive). If one of `-c` or `l`
is set, defaluts to `.` if empty (with `-r` implied).

## Dependencies

Requires `Rscript` to be installed and in the path.

Depends on:
- httr
- tidyverse
- wand

## Sample output

```
$ ./lpli -r
╔══════════════╤══════════════════════════════════════╗
║Class/Package │               Licenses               ║
╠══════════════╪══════════════════════════════════════╣
║amsmath       │The LaTeX Project Public License 1.3c ║
╟──────────────┼──────────────────────────────────────╢
║amssymb       │                                   NA ║
╟──────────────┼──────────────────────────────────────╢
║babel         │The LaTeX Project Public License 1.3  ║
╟──────────────┼──────────────────────────────────────╢
║ClearSans     │Apache License, version 2.0           ║
║              │The LaTeX Project Public License      ║
╟──────────────┼──────────────────────────────────────╢
║colortbl      │The LaTeX Project Public License      ║
╟──────────────┼──────────────────────────────────────╢
║datetime      │The LaTeX Project Public License 1.3  ║
╟──────────────┼──────────────────────────────────────╢
║diagbox       │The LaTeX Project Public License 1.3  ║
╟──────────────┼──────────────────────────────────────╢
║enumitem      │MIT License                           ║
╟──────────────┼──────────────────────────────────────╢
║fontawesome   │The LaTeX Project Public License 1.3  ║
╟──────────────┼──────────────────────────────────────╢
║fontenc       │The LaTeX Project Public License      ║
╟──────────────┼──────────────────────────────────────╢
║fourier       │The LaTeX Project Public License      ║
╟──────────────┼──────────────────────────────────────╢
║geometry      │The LaTeX Project Public License 1.3c ║
╟──────────────┼──────────────────────────────────────╢
║hyperref      │The LaTeX Project Public License 1.3  ║
╟──────────────┼──────────────────────────────────────╢
║inputenc      │The LaTeX Project Public License 1.3c ║
╟──────────────┼──────────────────────────────────────╢
║lettrine      │The LaTeX Project Public License 1.3  ║
╟──────────────┼──────────────────────────────────────╢
║marvosym      │The SIL Open Font License             ║
╟──────────────┼──────────────────────────────────────╢
║multimedia    │                                   NA ║
╟──────────────┼──────────────────────────────────────╢
║noto          │The LaTeX Project Public License      ║
║              │The SIL Open Font License             ║
╟──────────────┼──────────────────────────────────────╢
║parskip       │The LaTeX Project Public License 1.3c ║
╟──────────────┼──────────────────────────────────────╢
║pifont        │The LaTeX Project Public License 1.2  ║
╟──────────────┼──────────────────────────────────────╢
║pst-plot      │The LaTeX Project Public License      ║
╟──────────────┼──────────────────────────────────────╢
║pst-text      │The LaTeX Project Public License      ║
╟──────────────┼──────────────────────────────────────╢
║pstricks      │The LaTeX Project Public License 1.3  ║
╟──────────────┼──────────────────────────────────────╢
║ragged2e      │The LaTeX Project Public License 1.3c ║
╟──────────────┼──────────────────────────────────────╢
║tcolorbox     │The LaTeX Project Public License 1.3  ║
╟──────────────┼──────────────────────────────────────╢
║textpos       │The LaTeX Project Public License 1.3  ║
╟──────────────┼──────────────────────────────────────╢
║tikz          │Free Documentation License            ║
║              │The LaTeX Project Public License 1.3c ║
║              │GNU General Public License, version 2 ║
╟──────────────┼──────────────────────────────────────╢
║turnstile     │The LaTeX Project Public License      ║
╟──────────────┼──────────────────────────────────────╢
║wasysym       │The LaTeX Project Public License 1.3c ║
╟──────────────┼──────────────────────────────────────╢
║xcolor        │The LaTeX Project Public License 1.2  ║
╚══════════════╧══════════════════════════════════════╝
╔══════════════════════════════════════╤═════════════════╗
║               License                │Classes/Packages ║
╠══════════════════════════════════════╪═════════════════╣
║Apache License, version 2.0           │ClearSans        ║
╟──────────────────────────────────────┼─────────────────╢
║Free Documentation License            │tikz             ║
╟──────────────────────────────────────┼─────────────────╢
║GNU General Public License, version 2 │tikz             ║
╟──────────────────────────────────────┼─────────────────╢
║MIT License                           │enumitem         ║
╟──────────────────────────────────────┼─────────────────╢
║The LaTeX Project Public License      │ClearSans        ║
║                                      │colortbl         ║
║                                      │fontenc          ║
║                                      │fourier          ║
║                                      │noto             ║
║                                      │pst-plot         ║
║                                      │pst-text         ║
║                                      │turnstile        ║
╟──────────────────────────────────────┼─────────────────╢
║The LaTeX Project Public License 1.2  │pifont           ║
║                                      │xcolor           ║
╟──────────────────────────────────────┼─────────────────╢
║The LaTeX Project Public License 1.3  │babel            ║
║                                      │datetime         ║
║                                      │diagbox          ║
║                                      │fontawesome      ║
║                                      │hyperref         ║
║                                      │lettrine         ║
║                                      │pstricks         ║
║                                      │tcolorbox        ║
║                                      │textpos          ║
╟──────────────────────────────────────┼─────────────────╢
║The LaTeX Project Public License 1.3c │amsmath          ║
║                                      │geometry         ║
║                                      │inputenc         ║
║                                      │parskip          ║
║                                      │ragged2e         ║
║                                      │tikz             ║
║                                      │wasysym          ║
╟──────────────────────────────────────┼─────────────────╢
║The SIL Open Font License             │marvosym         ║
║                                      │noto             ║
╟──────────────────────────────────────┼─────────────────╢
║                                   NA │amssymb          ║
║                                      │multimedia       ║
╚══════════════════════════════════════╧═════════════════╝
```